"""Test RCR find."""

from __future__ import division

from rcr.calc_rcr import has_subscription


def ats(d):
    "add iso format TS to a date."
    return d + "T00:00:00.000000"


def test_integer_division_syntax_in_py2():
    assert 5 // 2 == 2


def test_two_services_with_one_cancel():
    # assuming of having two lists of objects for the same
    # object and check if there's a subscription
    terminated = [{"service": "s1", "terminated_date": ats("2019-12-05"),}]

    activated = [
        {"service": "s1", "activated_date": ats("2019-12-01"),},
        {"service": "s2", "activated_date": ats("2019-12-03"),},
    ]
    assert has_subscription(activated, terminated)


def test_cancel_another_service():
    terminated = [{"service": "s1", "terminated_date": ats("2019-12-05"),}]

    activated = [{"service": "s2", "activated_date": ats("2019-12-03"),}]
    assert has_subscription(activated, terminated)


def test_cancel_a_service():
    terminated = [{"service": "s1", "terminated_date": ats("2019-12-05"),}]
    activated = []
    assert not has_subscription(activated, terminated)


def test_cancel_same():
    terminated = [{"service": "s1", "terminated_date": ats("2019-12-05"),}]

    activated = [{"service": "s1", "activated_date": ats("2019-12-01"),}]
    assert not has_subscription(activated, terminated)


def test_activation_same():
    terminated = [{"service": "s1", "terminated_date": ats("2019-12-01"),}]

    activated = [{"service": "s1", "activated_date": ats("2019-12-05"),}]
    assert has_subscription(activated, terminated)


def test_cancel_two_activate_another():
    terminated = [
        {"service": "s1", "terminated_date": ats("2019-12-05"),},
        {"service": "s2", "terminated_date": ats("2019-12-04"),},
    ]
    activated = [{"service": "s3", "activated_date": ats("2019-12-06"),}]
    assert has_subscription(activated, terminated)


def test_cancel_two_but_activate_one_after():
    terminated = [
        {"service": "s1", "terminated_date": ats("2019-12-05"),},
        {"service": "s2", "terminated_date": ats("2019-12-04"),},
    ]
    activated = [{"service": "s2", "activated_date": ats("2019-12-06"),}]
    assert has_subscription(activated, terminated)


def test_cancel_two_but_activate_one_before():
    terminated = [
        {"service": "s1", "terminated_date": ats("2019-12-05"),},
        {"service": "s2", "terminated_date": ats("2019-12-10"),},
    ]
    activated = [{"service": "s2", "activated_date": ats("2019-12-06"),}]
    assert not has_subscription(activated, terminated)


def test_multi_activation_same_service():
    "This seems a rare corner case."
    terminated = [
        {"service": "s1", "terminated_date": ats("2019-12-05"),},
        {"service": "s2", "terminated_date": ats("2019-12-10"),},
    ]
    activated = [
        {"service": "s2", "activated_date": ats("2019-12-06"),},
        {"service": "s2", "activated_date": ats("2019-12-11"),},
    ]
    assert has_subscription(activated, terminated)


def test_empty_active_services():
    terminated = [
        {"service": "s1", "terminated_date": ats("2019-12-05"),},
        {"service": "s2", "terminated_date": ats("2019-12-10"),},
    ]
    assert not has_subscription([], terminated)


def test_no_active_services():
    terminated = [
        {"service": "s1", "terminated_date": ats("2019-12-05"),},
        {"service": "s2", "terminated_date": ats("2019-12-10"),},
    ]
    assert not has_subscription(None, terminated)


def test_empty_terminated():
    activated = [
        {"service": "s2", "activated_date": ats("2019-12-06"),},
        {"service": "s2", "activated_date": ats("2019-12-11"),},
    ]
    assert has_subscription(activated, [])


def test_no_terminated():
    activated = [
        {"service": "s2", "activated_date": ats("2019-12-06"),},
        {"service": "s2", "activated_date": ats("2019-12-11"),},
    ]
    assert has_subscription(activated, None)


def test_all_empty():
    assert not has_subscription([], [])


def test_all_none():
    assert not has_subscription(None, None)


def test_multi_switch_off_last_is_termination():
    terminated = [
        {"service": "s2", "terminated_date": ats("2019-12-06"),},
        {"service": "s2", "terminated_date": ats("2019-12-11"),},
    ]
    activated = [
        {"service": "s2", "activated_date": ats("2019-12-05"),},
        {"service": "s2", "activated_date": ats("2019-12-10"),},
    ]
    assert not has_subscription(activated, terminated)


def test_multi_switch_on_last_is_activation():
    terminated = [
        {"service": "s2", "terminated_date": ats("2019-12-05"),},
        {"service": "s2", "terminated_date": ats("2019-12-10"),},
    ]
    activated = [
        {"service": "s2", "activated_date": ats("2019-12-06"),},
        {"service": "s2", "activated_date": ats("2019-12-11"),},
    ]
    assert has_subscription(activated, terminated)


def test_multi_switch_on_last_is_activation_not_in_order():
    terminated = [
        {"service": "s2", "terminated_date": ats("2019-12-05"),},
        {"service": "s2", "terminated_date": ats("2019-12-10"),},
    ]
    activated = [
        {"service": "s2", "activated_date": ats("2019-12-11"),},
        {"service": "s2", "activated_date": ats("2019-12-06"),},
    ]
    assert has_subscription(activated, terminated)


def test_if_removing_status_breaks_things():
    terminated = [
        {
            "object": "xxx",
            "service": "s2",
            "activated_date": ats("2019-12-04"),
            "terminated_date": ats("2019-12-05"),
            "status": "Terminated",
        },
    ]
    # if I remove the status Activated in the order query
    # for activated objects I should get
    # the same object listed also in the list below
    activated = [
        {
            "object": "xxx",
            "service": "s2",
            "activated_date": ats("2019-12-04"),
            "terminated_date": ats("2019-12-05"),
            "status": "Terminated",
        },
        {
            "object": "xxx",
            "service": "s2",
            "terminated_date": None,
            "activated_date": ats("2019-12-15"),
            "status": "Activated",
        },
    ]
    assert has_subscription(activated, terminated)
