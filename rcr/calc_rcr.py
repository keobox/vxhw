"""Calculate Real Churn Rate over objects."""

from __future__ import print_function
from __future__ import division

from itertools import groupby
import argparse
import decimal
import requests


def group_by_key(key, things):
    """Returns a grouped dictionary."""
    d = {}
    for k, things in groupby(things, lambda t: t[key]):
        d[k] = list(things)
    return d


def calc_initial_subscribers_num(response):
    "Given report stat data, return subscribers at the beginning of the period."
    data = response["data"]
    n = 0
    for network_operator in data:
        groups = network_operator["object_group"]
        for group_val in groups.values():
            # assuming that the "active" under the group
            # is the sum of active under 'service_provider'
            # this is not well specified in assignment text
            # but I have verified this holds
            n = n + int(group_val.get("active", 0))
    return n


def zero_fill_month(month):
    "month:int returns filled str"
    if month < 10:
        return "0" + str(args.month)
    else:
        return str(month)


def invoke_stat_api(args):
    "Invoke report stat API."
    month = zero_fill_month(args.month)
    # use old style format string for backward compatibility
    payload = {
        "page": 1,
        "per_page": 30,
        "start": "%d-%s-01T00:00:00" % (args.year, month),
        "stop": "%d-%s-01T23:59:59" % (args.year, month),
        "last_of_the_day": "true",
        "api_key": args.apikey,
    }
    stat_query = "%s/api/v1/report/stat"
    query = stat_query % args.url
    response = requests.get(query, payload)
    return response.json()


def invoke_orders_api(args, query_format_string):
    "Invoke the order API with a query string"
    q = query_format_string % (args.year, zero_fill_month(args.month))
    payload = {
        "q": q,
        "api_key": args.apikey,
        "per_page": 1000,
        "page": 1,
    }
    orders_query = "%s/api/v1/order" % args.url
    response = requests.get(orders_query, payload)
    first_page = response.json()
    # Assuming first page is returned even if total is less than 1000
    yield first_page

    total = first_page["pagination"]["total"]
    if total > payload["per_page"]:
        per_page = first_page["pagination"]["per_page"]
        page_num = total // per_page
        last_per_page = total % per_page
        for page in range(2, page_num + 1):
            payload["page"] = page
            next_page = requests.get(orders_query, payload)
            yield next_page.json()

        payload["page"] = page + 1
        payload["per_page"] = last_per_page
        last_lage = requests.get(orders_query, payload)
        yield last_lage.json()


def has_subscription(activated_services, terminated_services):
    """
    Assuming activated_services and terminated_services are lists of objects
    contaning services for the same object id.
    Returns True if a subscription exists
    """
    if not activated_services:
        # object id has not activated services
        return False
    if not terminated_services:
        # object id has not terminated services
        return True
    matched_services = []
    activated_group = group_by_key("service", activated_services)
    terminated_group = group_by_key("service", terminated_services)
    for service, activation_list in activated_group.items():
        if service in terminated_group:
            # search for re-activations
            termination_list = terminated_group[service]
            max_activation = max(a["activated_date"] for a in activation_list)
            max_termination = max(t["terminated_date"] for t in termination_list)
            matched_services.append(max_activation > max_termination)
        else:
            # another service is active in the period
            matched_services.append(True)
    return any(matched_services)


def algorithm(activated_objs, terminated_objs, terminated_num):
    """
       This is the algorithm to check if the
       terminated objects have an active subscription
       in which case a terminated object should not be counted
       because a customer has an active service.

       In coarse grain the algorithm can be described in this way:
       1. Given a terminated objects list and an activated objects list
          for both the lists a groupby on the 'object' field is processed
          this constructs two dictionaries where keys are the object ids.
          
       2. Then for every 'object' the has_subcription function checks
          if an object has another 'service' active or
          there was another activation on the same service
    """
    # group orders by objects id
    terminated_groupby = group_by_key("object", terminated_objs)
    terminated_objs = None
    activated_groupby = group_by_key("object", activated_objs)
    activated_objs = None
    # Adjustment algorithm. Check tests in rcr/tests/test_rcr.py
    for obj_id, obj_list in terminated_groupby.items():
        if has_subscription(activated_groupby.get(obj_id, []), obj_list):
            terminated_num = terminated_num - 1
    return terminated_num


def main(args):
    "Main function."
    resp = invoke_stat_api(args)
    subscribers_num = calc_initial_subscribers_num(resp)
    print("Finish to retrieve subscribers")

    terminated_objs = []
    for resp in invoke_orders_api(args, "terminated_date:/%d-%s/,status:Terminated"):
        terminated_objs.extend(resp["data"])
    terminated_num = len(terminated_objs)
    print("Raw terminated objects", terminated_num)
    # invoke_orders_api(args, "activated_date:/%d-%s/,status:Activated")
    activated_objs = []
    for resp in invoke_orders_api(args, "activated_date:/%d-%s/"):
        activated_objs.extend(resp["data"])
    print("Finish to retrieve orders")

    terminated_num = algorithm(activated_objs, terminated_objs, terminated_num)

    print("Number of subscribers", subscribers_num)
    print("Adjusted terminated number", terminated_num)
    print("RCR", decimal.Decimal(terminated_num) / subscribers_num * 100, "%")


def check_month(val):
    "Checks for valid month."
    i = int(val)
    if i < 1 or i > 12:
        raise argparse.ArgumentTypeError("%s invalid month" % val)
    return i


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Home work")
    parser.add_argument("year", type=int, help="Year")
    parser.add_argument("month", type=check_month, help="Month")
    parser.add_argument("url", type=str, help="API url to use")
    parser.add_argument("apikey", type=str, help="API Key")
    args = parser.parse_args()
    main(args)
