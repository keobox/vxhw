# Home work #

### Installation ###

* Is necessary to have access to a RESTful API for this to run.
* Install a virtualenv for this project
* `pip install -r requirements.txt`

### To Run in production ###
Write an "api.env" file like the following template

`export VX_API_URL="https://your_api_url.org"`  
`export VX_API_KEY="YOURKEY"`

then invoke  
`./launch_rcr.sh year month`

### To run unit tests for Python 2.7 and 3.7 ###
`pip install -r requirements_test.txt`  
`tox`

The unit test procedure was tested on a Mac.
