#!/bin/sh

set -o errexit

. api.env

if [ $# -eq 2 ] ; then
    cd rcr
    python calc_rcr.py $1 $2 $VX_API_URL $VX_API_KEY
else
    echo "Usage:"
    echo "$0 year month"
fi
