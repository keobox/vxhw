#!/usr/bin/env python

from setuptools import setup, find_packages

setup (
    name='vxhw',
    version='1.0',
    author='Cesare Placanica',
    author_email='keobox@gmail.com',
    license='MIT',
    packages=find_packages(),
    description='Homework',
    classifiers=(
    ),
    install_requires=[],
    scripts=[]
)
